# PracticasSA

Practicas de laboratorio de Software Avanzado Segundo Semestre 2020

Universidad San Carlos de Guatemala

### Ramas Creadas
> Practica 1 - Creacion SOAP Client 

> Practica 2 - Uso de Autenticacion 

> Practica 3 - Aplicacion Restaurante

> Practica 4 - Extension de Aplicacion Restaurante, Implementacion ESB

> Practica 8 - Docker-Compose con web server y base de datos

> Practica 9 - Docker-Compose y Volumenes